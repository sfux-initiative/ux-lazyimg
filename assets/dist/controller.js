import { Controller } from 'stimulus';
export default class extends Controller {
  connect() {
    // Load HD in background
    const hd = new Image(); // Once loaded, replace image

    hd.addEventListener('load', () => {
      this.element.src = this.element.getAttribute('data-hd-src');
    });
    hd.src = this.element.getAttribute('data-hd-src');
  }

}