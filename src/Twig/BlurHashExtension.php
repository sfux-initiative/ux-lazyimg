<?php

namespace Symfony\UX\Lazyimg\Twig;

use kornrunner\Blurhash\Blurhash;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class BlurHashExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('blur_hash', [$this, 'blurHash']),
        ];
    }

    public function blurHash(string $filename): string
    {
        $image = imagecreatefromstring(file_get_contents($filename));
        $width = imagesx($image);
        $height = imagesy($image);

        $pixels = [];
        for ($y = 0; $y < $height; ++$y) {
            $row = [];
            for ($x = 0; $x < $width; ++$x) {
                $index = imagecolorat($image, $x, $y);
                $colors = imagecolorsforindex($image, $index);

                $row[] = [$colors['red'], $colors['green'], $colors['blue']];
            }

            $pixels[] = $row;
        }

        $components_x = 4;
        $components_y = 3;

        return Blurhash::encode($pixels, $components_x, $components_y);
    }
}
